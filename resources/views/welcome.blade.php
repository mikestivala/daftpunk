<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="We'll find out whether Daft Punk are touring so that you don't have to!">
        <meta name="og:image" content="{{ asset('/images/daftpunk.jpg') }}"/>

        <title>Are Daft Punk Touring Yet?</title>
        
        <!-- Styles -->
        <style> 
            body {
              background: url(/images/daftpunk.jpg) no-repeat center center fixed;
              -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;
            }
        </style>
    </head>
    <body>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-63317773-2', 'auto');
          ga('send', 'pageview');

        </script>
    </body>
</html>
